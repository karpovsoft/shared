Co-operation/ joint-purchases app.

Angular/Symfony/API Platform framework

Features:
Multi-language support for UI
Client and server RBAC
Unitests 

---------

Приложение для расчетов при совместных покупках, ведении совместного бизнеса и т.д.
Наипсано на Angular/Symfony/API Platform framework

В приложении реализованы:
Поддержка многоязычного интерфейса для пользовательского интерфейса.
Проверка доступа на базе ролей на клиенте и сервере.
Юнитесты на бизнес-логику
