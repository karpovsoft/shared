<?php
/**
 * @author Alex Karpov
 */

namespace App\Business;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract  class AbstractApiController extends AbstractController
{
    public function apiJson(mixed $data, int $status = 200, array $headers = [], array $context = []) : JsonResponse
    {
        $headers =   array_merge( $headers, [
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => "*"
        ]);

        return $this->json($data, $status, $headers, $context);
    }
}