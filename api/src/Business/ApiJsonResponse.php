<?php

namespace App\Business;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiJsonResponse extends JsonResponse
{
    public function __construct($data)
    {
        parent::__construct($data, 200,
            [
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => "*"
            ]);
    }
}