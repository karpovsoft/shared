<?php
/**
 * @author Alex Karpov
 */

namespace App\Business;

use App\Repository\TeamRepository;
use App\Entity\Team;

class CurrentTeam
{
    private TeamRepository $repo ;
    public function __construct(TeamRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getId() : int
    {
        return $this->repo->getDefaultTeamId();
    }

    public function get() : Team
    {
        return $this->repo->find($this->getId());
    }

}