<?php
/**
 * Default campaign resolver
 * @author Alex Karpov
 */

namespace App\Business;

use App\Entity\Campaign;
use App\Repository\CampaignRepository;

class DefaultCampaign
{
    /** @var CampaignRepository */
    private CampaignRepository $repo;

    /**
     * DefaultCampaign constructor.
     * @param $teamId
     */
    public function __construct(CampaignRepository $repo)
    {
        $this->repo = $repo;
    }


    public function getId($teamId) : ?int
    {
        return $this->repo->getDefaultCampaignId($teamId);
    }

    public function get($teamId) : ?Campaign
    {
        return $this->repo->find($this->getId($teamId));
    }

}