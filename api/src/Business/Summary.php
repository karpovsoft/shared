<?php
/**
 * @author Alex Karpov
 */

namespace App\Business;


use App\Calculation\CalculationStrategyFactory;
use App\Repository\OperationRepository;
use App\Repository\PartnerRepository;

class Summary
{
    private PartnerRepository $partnerRepo;
    private OperationRepository $operationRepo;
    private CurrentTeam $team;
    private DefaultCampaign $defaultCampaign;

    public function __construct(
        PartnerRepository $partnerRepository,
        OperationRepository $operationRepository,
        CurrentTeam $team,
        DefaultCampaign $campaign
    )
    {
        $this->partnerRepo = $partnerRepository;
        $this->operationRepo = $operationRepository;
        $this->team = $team;
        $this->defaultCampaign = $campaign;
    }

    /**
     * @return SummaryDTO
     */
    public function summary() : SummaryDTO
    {
        $campaign = $this->defaultCampaign->get($this->team->getId());
        $defaultCampaignId = $campaign->getId();
        // totals
        $totalIncome = $this->operationRepo->incomeByCampaignId($defaultCampaignId);
        $totalExpenses = $this->operationRepo->expenseByCampaignId($defaultCampaignId);

        // totals groupped by ids
        $incomeByIds = $this->operationRepo->incomeTotals($defaultCampaignId);
        $expensesByIds = $this->operationRepo->expenseTotals($defaultCampaignId);

        // Calculate balance by ids
        $strategy = new CalculationStrategyFactory();
        $totalsById = $strategy->getStrategy($campaign)->calculate($incomeByIds, $expensesByIds);

        $partners = $this->partnerRepo->getTeamPartnersForTotal($this->team->getId());
        // statistics fy partner
        $partnerTotals = (new Totals())->mergeTotals($partners, $totalsById);

        return new SummaryDTO($totalIncome, $totalExpenses,
            $this->partnerRepo->countByTeamId($this->team->getId()),
            $partnerTotals
        );
    }

}

class SummaryDTO
{
    public function __construct(public int $totalIncome,
        public int $totalExpenses,
        public int $totalPartners,
        public iterable $partnerTotals
    ) {}
}
