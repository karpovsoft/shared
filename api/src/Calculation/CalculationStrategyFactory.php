<?php
/**
 * @author Alex Karpov
 */

namespace App\Calculation;


use App\Entity\Campaign;

class CalculationStrategyFactory
{
    public function getStrategy(Campaign $campaign)
    {
        // returns only even sharing strategy for now
        // everyone gets the same
        return new EvenCalculationStrategy();
    }
}