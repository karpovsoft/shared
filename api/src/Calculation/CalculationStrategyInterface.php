<?php
/**
 * @author Alex Karpov
 */

namespace App\Calculation;


interface CalculationStrategyInterface
{
    /**
     *  Returns how much each partner owes or must get groupped by id
     * @param IdTotal[] $incomesById
     * @param IdTotal[] $expensesById
     * @return IdTotal[]
     */
    public function calculate(iterable $incomesById, iterable $expensesById) : iterable;

}