<?php
/**
 * @author Alex Karpov
 */

namespace App\Calculation;


use phpDocumentor\Reflection\Types\Iterable_;

class EvenCalculationStrategy implements CalculationStrategyInterface
{
    public function calculate(iterable $incomesById, iterable $expensesById) : iterable
    {
        $totalIncomes = array_reduce($incomesById, function($carry, $item) {
            /** @var IdTotal $item */
            $carry += $item->getTotal();

            return $carry;
        }, 0);

        $totalExpenses = array_reduce($expensesById, function($carry, $item) {
            /** @var IdTotal $item */
            $carry += $item->getTotal();

            return $carry;
        }, 0);
        // ids of partners
        $uniqueIds = array_values(
            array_unique(array_merge(
                array_map(function($item) {
                    /** @var IdTotal $item */
                    return $item->getId();
                }, $incomesById),

                array_map(function($item) {
                    /** @var IdTotal $item */
                    return $item->getId();
                }, $expensesById)
            ))
        );

        $partnerClearance = [];

        $middle = ($totalExpenses + $totalIncomes)/count($uniqueIds);

        // get each partner's part
        foreach ($uniqueIds as $id)
        {
            $partnerIncomes = $this->getSumById($incomesById, $id);
            $partnerExpenses = $this->getSumById($expensesById, $id);

            // $partnerExpenses are negative
            $partnerBalance = $middle - $partnerExpenses - $partnerIncomes;

            $partnerClearance[] = new IdTotal($id, $partnerBalance);
        }

        return $partnerClearance;
    }

    /**
     * @param IdTotal[] $collection
     * @return int
     */
    private function getSumById(iterable $collection, int $id) : int
    {
        $filtered = array_filter($collection, function ($item) use ($id) {
            /** @var IdTotal $item */
            return ($item->getId() == $id);
        });

        $val = array_reduce( $filtered
            , function($carry, $item) {
                /** @var IdTotal $item */
                $carry += $item->getTotal();
                return $carry;
        }, 0);

        return $val;
    }

}