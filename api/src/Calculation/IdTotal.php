<?php
/**
 * Created by PhpStorm.
 * User: AlexKarpov
 * Date: 05.11.2022
 * Time: 8:00
 */

namespace App\Calculation;


class IdTotal
{
    private int $id;

    private int $total;


    /**
     * IdTotal constructor.
     */
    public function __construct($id, $total)
    {
        $this->id = $id;
        $this->total = $total;
    }

    public function  getId() : int
    {
        return $this->id;
    }

    public function getTotal() : int
    {
        return $this->total;
    }
}