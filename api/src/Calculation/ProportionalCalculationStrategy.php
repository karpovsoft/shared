<?php
/**
 * @author Alex Karpov
 */

namespace App\Calculation;

/**
 * Class ProportionalCalculationStrategy
 * @package App\Calculation
 *
 * Every partner gains money in a propotion of his(her) expenses
 */
class ProportionalCalculationStrategy implements CalculationStrategyInterface
{

    public function calculate(iterable $incomesById, iterable $expensesById) : iterable
    {
        throw new \Exception("Not implemented yet");
    }

}