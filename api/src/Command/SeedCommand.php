<?php
/**
 * @author AlexKarpov
 */

namespace App\Command;

use App\Entity\Partner;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\Campaign;
use App\Entity\Team;
use App\Entity\User;

// the "name" and "description" arguments of AsCommand replace the
// static $defaultName and $defaultDescription properties
#[AsCommand(
name: 'app:seed',
    description: 'Creates a default team and user.',
    hidden: false
)]

class SeedCommand extends Command
{
    private EntityManagerInterface $manager;
    private UserPasswordHasherInterface $hasher;

    public function __construct(EntityManagerInterface $manager,
                                UserPasswordHasherInterface $hasher)
    {
        $this->manager = $manager;
        $this->hasher = $hasher;
        parent::__construct(null);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ////// enter your data /////////
        $email = "karpovsoft@yandex.ru";
        $first = "Alex";
        $last = "Karpov";
        $password = "mountainski";
        ///////////////////////////////

        $team = new Team();
        $team->setName("Default Team");

        $user = new User();
        $user->setFirstName($first);
        $user->setLastName($last);
        $user->setEmail($email);
        $user->setTeam($team);
        $password = $this->hasher->hashPassword($user, $password);
        $user->setPassword($password);
        $user->setRoles(["ROLE_ADMIN"]);

        $partner = new Partner();
        $partner->setTeam($team);
        $partner->setUser($user);
        $partner->setName($first." ".$last);

        $campaign = new Campaign();
        $campaign->setTeam($team);
        $campaign->setIsClosed(false);
        $campaign->setName("Default Campaign");

        $this->manager->persist($team);
        $this->manager->persist($user);
        $this->manager->persist($campaign);
        $this->manager->persist($partner);

        $this->manager->flush();
        $output->writeln("Default objects are created");
        return Command::SUCCESS;
    }
}