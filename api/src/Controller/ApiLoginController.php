<?php

namespace App\Controller;

use App\Business\AbstractApiController;
use App\Entity\ApiToken;
use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Routing\Annotation\Route;

class ApiLoginController extends AbstractApiController
{
    #[Route('/api/login', name: 'api_login')]
    public function index(ApiTokenRepository $repository, #[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
             return $this->apiJson([
                     'message' => 'missing credentials',
                 ], Response::HTTP_UNAUTHORIZED);
        }

        $token = $repository->findOneByUserId($user->getId());
        if(null === $token) {
            $token = new ApiToken();
        }
        $token->setToken(bin2hex(random_bytes(50)));
        $token->setUser($user);
        $repository->add($token, true);

        return $this->apiJson([
           'user'  => $user->getUserIdentifier(),
           'token' => $token->getToken(),
            'roles' => $user->getRoles()
        ]);
    }

    #[Route('/api/logout', name: 'api_logout')]
    public function logout(ApiTokenRepository $repository, #[CurrentUser] ?User $user): Response
    {
        if (null !== $user) {
            $token = $repository->findOneByUserId($user->getId());
            if(null !== $token)
            {
                $repository->remove($token, true);
            }
        }

        return $this->apiJson([
            'message' => 'logged out',
        ]);
    }
}