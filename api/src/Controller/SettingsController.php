<?php
/**
 * @author Alex Karpov
 */

namespace App\Controller;

use App\Business\AbstractApiController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Business\CurrentTeam;
use App\Business\DefaultCampaign;

class SettingsController extends AbstractApiController 
{
    #[Route("/api/settings", name : 'settings')]
    public function index(CurrentTeam $team, DefaultCampaign $campaign) : JsonResponse
    {
        $teamId = $team->getId();
        $campaignId = $campaign->get($teamId)->getId();

        return $this->apiJson([
            'teamId' => $teamId,
            'campaignId' => $campaignId,
            'campaignIri' => sprintf("/api/campaigns/%d", $campaignId)
        ]);
    }
}