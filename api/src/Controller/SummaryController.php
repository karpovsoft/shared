<?php
/**
 * @author Alex Karpov
 */


namespace App\Controller;

use App\Business\AbstractApiController;
use App\Business\CurrentTeam;
use App\Business\DefaultCampaign;
use App\Repository\OperationRepository;
use App\Repository\PartnerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Business\Summary;

class SummaryController extends AbstractApiController
{
    private Summary $manager;

    public function __construct(
            PartnerRepository $partnerRepo,
            OperationRepository $operationRepo,
            CurrentTeam $curTeam,
            DefaultCampaign $defaultCampaign)
    {
        $this->manager = new Summary($partnerRepo, $operationRepo, $curTeam, $defaultCampaign);
    }

    #[Route("/api/summary", name : 'summary')]
    public function index() : JsonResponse
    {
        return $this->apiJson($this->manager->summary());
    }

    #[Route("/api/history", name : 'history')]
    public function history() : JsonResponse
    {
        return $this->apiJson($this->manager->history());
    }
}