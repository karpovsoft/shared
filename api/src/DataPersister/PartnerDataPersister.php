<?php
/**
 * @author Alex Karpov
  */

namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Business\CurrentTeam;
use App\Entity\Partner;
use Doctrine\ORM\EntityManagerInterface;

class PartnerDataPersister implements DataPersisterInterface
{
    private CurrentTeam $team;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, CurrentTeam $team)
    {
        $this->entityManager = $entityManager;
        $this->team = $team;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Partner;
    }

    public function persist($data, array $context = [])
    {
        $data->setTeam($this->team->get());
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        return $data;
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}