<?php

namespace App\Entity;

use App\Repository\OperationRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;

#[ORM\Entity(repositoryClass: OperationRepository::class)]
#[ApiResource(
    normalizationContext : ['groups' => ['operations.read']],
    collectionOperations: [
        'post' => ['security' => 'is_granted("ROLE_ADMIN")']
    ],
    itemOperations : [
        'get' => ['security' => 'is_granted("ROLE_ADMIN")'],
        'delete' => ['security' => 'is_granted("ROLE_ADMIN")']
    ]
)]
#[ApiFilter(OrderFilter::class, properties: ['operationDate'])]
class Operation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[
        ORM\Column(type: 'integer'),
        Groups(['operations.read'])
    ]
    private $id;

    #[
        ORM\Column(type: 'integer'),
        Groups(['operations.read'])
    ]
    private $sum;

    #[ORM\ManyToOne(targetEntity: Campaign::class, inversedBy: 'operations')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['operations.read'])]
    private $campaign;

    #[ORM\ManyToOne(targetEntity: Partner::class, inversedBy: 'operations')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['operations.read'])]
    private $partner;

    #[ORM\Column(type: 'date')]
    #[Groups(['operations.read'])]
    private $operationDate;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['operations.read'])]
    private $comments;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSum(): ?int
    {
        return $this->sum;
    }

    public function setSum(int $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function getOperationDate(): ?\DateTimeInterface
    {
        return $this->operationDate;
    }

    public function setOperationDate(\DateTimeInterface $operationDate): self
    {
        $this->operationDate = $operationDate;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }
}
