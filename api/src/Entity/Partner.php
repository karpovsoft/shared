<?php
/**
 * @author Alex Karpov
 */

namespace App\Entity;

use App\Repository\PartnerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity(repositoryClass: PartnerRepository::class)]
#[ApiResource(
    denormalizationContext : ['groups' => ['partner.write']],
    collectionOperations: [
        'post' => ['security' => 'is_granted("ROLE_ADMIN")']
    ],
    itemOperations : [
        'get' => ['security' => 'is_granted("ROLE_ADMIN")']
    ]
)]
class Partner
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $user;

    #[
        ORM\Column(type: 'string', length: 255),
        Groups(['partner.write', 'operations.read']),
        Assert\NotBlank
    ]
    private $name;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'partners')]
    #[ORM\JoinColumn(nullable: false)]
    private $team;

    #[ORM\OneToMany(mappedBy: 'partner', targetEntity: Operation::class)]
    #[ApiSubresource]
    private $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return Collection<int, Operation>
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setPartner($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getPartner() === $this) {
                $operation->setPartner(null);
            }
        }

        return $this;
    }
}
