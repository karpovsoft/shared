<?php

namespace App\Repository;

use App\Calculation\IdTotal;
use App\Entity\Operation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Iterable_;

/**
 * @extends ServiceEntityRepository<Operation>
 *
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }

    public function add(Operation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Operation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function incomeByCampaignId($campaignId): int
    {
        return $this->createQueryBuilder('o')
            ->where("o.sum>0")
            ->andWhere('o.campaign = :val')
            ->setParameter('val', $campaignId)
            ->select("sum(o.sum) as total")
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function expenseByCampaignId($campaignId): int
    {
        return $this->createQueryBuilder('o')
            ->where("o.sum<0")
            ->andWhere('o.campaign = :val')
            ->setParameter('val', $campaignId)
            ->select("-sum(o.sum) as total")
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param int $campaignId
     * @return IdTotal[]
     */
    public function incomeTotals($campaignId) : iterable
    {
        $result = $this->createQueryBuilder('o')
            ->where("o.sum>0")
            ->andWhere('o.campaign = :val')
            ->setParameter('val', $campaignId)
            ->select("partner.id, sum(o.sum) as total")
            ->join("o.partner", "partner")
            ->groupBy("partner")
            ->getQuery()
            ->getArrayResult()
            ;

        return array_map(function($val)  {
                return new IdTotal($val["id"], $val["total"]);
            }, $result);
    }

    /**
     * @param int $campaignId
     * @return IdTotal[]
     */
    public function expenseTotals($campaignId): iterable
    {
        $result = $this->createQueryBuilder('o')
            ->where("o.sum<0")
            ->andWhere('o.campaign = :val')
            ->setParameter('val', $campaignId)
            ->select("partner.id, sum(o.sum) as total")
            ->join("o.partner", "partner")
            ->groupBy("o.partner")
            ->getQuery()
            ->getArrayResult()
            ;

        return array_map(function($val)  {
            return new IdTotal($val["id"], $val["total"]);
        }, $result);
    }

}
