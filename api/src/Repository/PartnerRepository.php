<?php
/**
 * @author Alex Karpov
 */

namespace App\Repository;

use App\Business\PartnerTotal;
use App\Entity\Partner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Partner>
 *
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partner::class);
    }

    public function add(Partner $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Partner $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countByTeamId($teamId): int
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.team = :val')
            ->setParameter('val', $teamId)
            ->select("COUNT(p.id) as count_partners")
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @param int $teamId
     * @return ParterTotal[] list of partners for total calculation
     */
    public function getTeamPartnersForTotal($teamId): iterable
    {
        return
            array_map(function($el) {
                return new PartnerTotal($el["id"], $el["name"]);
            },
                $this->createQueryBuilder('p')
                    ->andWhere('p.team = :val')
                    ->setParameter('val', $teamId)
                    ->select("p.id, p.name")
                    ->getQuery()
                    ->getArrayResult()
            );
    }

}
