<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use App\Calculation;

final class EvenCalculationStrategyTest extends TestCase
{
    /**
     * @param IdTotal[] $collection
     * @return int
     */
    private function getById(iterable $collection, int $id) : int
    {
        $els = array_values(
            array_filter($collection, function ($item) use ($id) {
                return ($item->getId() == $id);
            })
        );
        if(count($els) > 0) {
            return $els[0]->getTotal();
        }

        return 0;
    }

    public function testCalculate(): void
    {
        $strategy = new Calculation\EvenCalculationStrategy();

        $partnerIncomes = [
            new Calculation\IdTotal(1, 100),
            new Calculation\IdTotal(1, 210),
            new Calculation\IdTotal(2, 150),
        ];

        $partnerExpenses = [
            new Calculation\IdTotal(1, -50),
            new Calculation\IdTotal(3, -200),
            new Calculation\IdTotal(3, -150),
        ];

        $partnerClearance = $strategy->calculate($partnerIncomes, $partnerExpenses);

        $partner_1_balance = $this->getById($partnerClearance, 1);
        $this->assertSame(-240, $partner_1_balance);

        $partner_2_balance = $this->getById($partnerClearance, 2);
        $this->assertSame(-130, $partner_2_balance);

        $partner_3_balance = $this->getById($partnerClearance, 3);
        $this->assertSame(370, $partner_3_balance);

    }
}