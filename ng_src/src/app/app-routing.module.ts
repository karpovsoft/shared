import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SummaryComponent} from './summary/summary.component'
import { LoginComponent} from './login/login.component'
import {LogoutComponent} from "./logout/logout.component";
import {EditPartnerComponent} from "./edit-partner/edit-partner.component";
import {AdminGuard} from "./auth/admin.guard";

const routes: Routes = [
  { path: '', component: SummaryComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
