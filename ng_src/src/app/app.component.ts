import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { Title } from '@angular/platform-browser';
import {CurrentUserService} from "./services/currentuser.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private translate: TranslateService,
              private title: Title,
              public currentUser: CurrentUserService,
              public router: Router) {
    translate.setDefaultLang('en');
    translate.addLangs(['en', 'ru']);
    translate.use('ru');
    translate.get("appTitle").subscribe((res: string) => {
      title.setTitle(res);
    });
  }
}
