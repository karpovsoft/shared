import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {CurrentUserService} from "../services/currentuser.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private currentUser: CurrentUserService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.currentUser.getToken();
    const apiKey : string =  token== null ? "" : token;
    if(token != null) {
      request = request.clone({
        headers: request.headers.append(
          'X-AUTH-TOKEN', apiKey
        )
      })
    }
    return next.handle(request);
  }
}
