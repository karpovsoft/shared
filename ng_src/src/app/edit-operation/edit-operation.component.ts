import { Component, OnInit, Input } from '@angular/core';
import {PartnerService} from "../services/partner.service";
import {CurrentUserService} from "../services/currentuser.service";
import {Partner} from "../edit-partner/edit-partner.component";
import {MatDialogRef} from "@angular/material/dialog";
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {OperationService} from "../services/operation.service";
import {AlertsService} from "../services/alerts.service";

enum OperationType {
  Got = 1,
  Spent = 2
}

export interface Operation {
  iri : string
  campaign : string // IRI
  partner : string  // IRI
  sum : number
  operationDate : Date
  comments : string
};

@Component({
  selector: 'app-edit-operation',
  templateUrl: './edit-operation.component.html',
  styleUrls: ['./edit-operation.component.css']
})
export class EditOperationComponent implements OnInit {

  constructor(private partnerService : PartnerService,
              private currentUser: CurrentUserService,
              public dialogRef: MatDialogRef<EditOperationComponent>,
              private formBuilder: FormBuilder,
              private operationService : OperationService,
              public alerts: AlertsService) { }

  @Input()
  public id : number = -1;

  public operationForm: FormGroup = new FormGroup({});
  public partners : any;

  ngOnInit(): void {

    const settings = this.currentUser.getSettings();

    this.operationForm = this.formBuilder.group({
      partner: ["", Validators.required],
      campaign: (settings != null)? settings.campaignIri : "",
      operationType: "",
      operationDate : ["", Validators.required],
      sum: [0, Validators.required],
      comments : ""
    });

    if(settings != null)
    {
      this.partnerService.getTeamPartners(settings.teamId).subscribe((resp :any) => {
        this.partners = resp;
      })
    }
  }

  onSubmit()
  {
    const operatioin : Operation = this.operationForm.value;
    operatioin.sum = parseInt(this.operationForm.value['sum']);
    if(this.operationForm.value['operationType'] == OperationType.Spent) {
      operatioin.sum = -operatioin.sum;
    }

    if(this.operationForm.controls['partner'].errors == null &&
        this.operationForm.controls['sum'].errors == null &&
        this.operationForm.controls['operationDate'].errors == null)
    {
      this.operationService.createOperation(operatioin).subscribe(resp => {
            this.dialogRef.close("save");
          },
          error => {
            alert(error.message);
          });
    }

  }
}
