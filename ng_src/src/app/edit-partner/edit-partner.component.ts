import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {AlertsService} from "../services/alerts.service";
import {TranslateService} from "@ngx-translate/core";
import {PartnerService} from "../services/partner.service";
import {MatDialogRef} from "@angular/material/dialog";

export interface Partner {
  iri : string
  name : string
};

@Component({
  selector: 'app-edit-partner',
  templateUrl: './edit-partner.component.html',
  styleUrls: ['./edit-partner.component.css']
})
export class EditPartnerComponent implements OnInit {
  @Input()
  public id : number = -1;

  public item: Partner|null = null;

  public title: string = "";
  public partnerForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              public alerts: AlertsService,
              private translate: TranslateService,
              private service: PartnerService,
              public dialogRef: MatDialogRef<EditPartnerComponent>) {

    this.partnerForm = this.formBuilder.group({
      name: ["", Validators.required],
    });
  }

  ngOnInit(): void {
    let v = (this.id == -1)? this.translate.get("summary.addPartner") : this.translate.get("summary.editPartner");
    v.subscribe( res => this.title = res);
  }

  onSubmit() {
    const partner : Partner = this.partnerForm.value;
    if(this.partnerForm.controls['name'].errors == null)
    {
      this.service.createParter(partner).subscribe(resp => {
            this.dialogRef.close("save");
          },
          error => {
            alert(error.message);
          });
    }
  }

  onCancel()
  {
    this.dialogRef.close();
  }

}
