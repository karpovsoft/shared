import {Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {OperationService, OperationExtended} from "../services/operation.service";
import {CurrentUserService} from "../services/currentuser.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit, OnChanges {

  @Input() reloadOperations : number = Date.now();
  @Output() operationDeleted : EventEmitter<boolean> = new EventEmitter<boolean>();

  displayedColumns: string[] = ['date', 'incomeSum', 'expenseSum', 'partner', 'notes', 'actions'];
  public operations : OperationExtended[] = [];

  constructor(
      private service: OperationService,
      private snackBar: MatSnackBar,
      public currentUser : CurrentUserService) { }

  private reload()
  {
    const settings = this.currentUser.getSettings();
    if(settings != null) {
      this.service.getCampaignOperations(settings.campaignId).subscribe(
          resp =>  {
            this.operations = resp;
          },
          error => {
            this.snackBar.open(error.message);
          }
      )

    }
  }

  public deleteItem(row: any)
  {
    if(confirm("Are you sure?")) {
      this.service.deleteOperation(row["iri"]).subscribe(resp => {
        this.snackBar.open("Deleted");
        this.operationDeleted.emit(true);
      }, error => {
        this.snackBar.open(error.message);
      })
    }
  }

  ngOnInit(): void {
    this.reload();
  }

  ngOnChanges()
  {
    this.reload();
  }
}
