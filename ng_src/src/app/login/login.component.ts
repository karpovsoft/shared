import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import  { AuthService } from "../services/auth.service"
import {CurrentUserService} from "../services/currentuser.service";
import {Router} from "@angular/router";
import {AlertsService} from "../services/alerts.service";

export interface CurrentUser
{
    token: string,
    roles: Array<string>
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private auth: AuthService,
              private currentUser: CurrentUserService,
              private router : Router,
              public alerts : AlertsService) {

      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', [Validators.required, Validators.minLength(6)]]
      });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.auth.login(this.loginForm.controls['username'].value, this.loginForm.controls['password'].value)
        .subscribe( resp => {
            this.currentUser.save(resp);
            this.router.navigate(['/'])
        })
  }

  getMinLengthErrorMessage() {
      return "Password must be at least 6 characters long";
  }
}
