import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from "../services/currentuser.service";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
      private currentUser: CurrentUserService,
      private auth: AuthService,
      private router: Router) { }

  ngOnInit(): void {
    this.logout();
  }

  logout()
  {
    this.auth.logout().subscribe( resp => {
      this.router.navigate(['/']);
    })
    this.currentUser.clear();
  }
}
