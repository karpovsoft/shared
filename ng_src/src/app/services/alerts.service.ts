import { Injectable } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(private translate: TranslateService) { }

  getRequiredErrorMessage() {
      return this.translate.instant("form.requiredField");
  }

}
