import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, of } from 'rxjs'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string) : Observable<any> {
    let user = {username : username, password: password};
    return this.http.post<any>("/api/login", user);
  }

  logout() : Observable<any> {
    return this.http.get<any>("/api/logout");
  }

}
