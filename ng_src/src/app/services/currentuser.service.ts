import { Injectable } from '@angular/core';
import {CurrentUser} from "../login/login.component";

export interface Settings {
  teamId: number,
  campaignId: number,
  campaignIri : string
}

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  constructor() { }

  save(user: CurrentUser)
  {
    localStorage.setItem("user-token", user.token);
    localStorage.setItem("user-roles", JSON.stringify(user.roles));
  }

  getSettings() : Settings | null
  {
    let settings = localStorage.getItem("user-settings");
    return settings == null ? null : JSON.parse(settings);
  }

  saveSettings(settings: Settings)
  {
    localStorage.setItem("user-settings", JSON.stringify(settings));
  }

  clear()
  {
    localStorage.removeItem("user-token");
    localStorage.removeItem("user-roles");
    localStorage.removeItem("user-settings");
  }

  getToken() : string | null
  {
    return localStorage.getItem("user-token");
  }

  getRoles() : Array<string>
  {
    let roles = localStorage.getItem("user-roles");
    return roles == null ? new Array<string>() : JSON.parse(roles);
  }

  isLoggedIn() : boolean
  {
    return (this.getToken() != null)
  }

  hasRole(role : string) : boolean
  {
    let roles = this.getRoles();
    return (roles.indexOf(role)!= -1)
  }
}
