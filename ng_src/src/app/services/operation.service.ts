import { Injectable } from '@angular/core';
import {Operation} from "../edit-operation/edit-operation.component";
import {HttpClient} from "@angular/common/http";
import {pluck, map} from "rxjs/operators";
import {Observable} from "rxjs";

export interface OperationExtended extends Operation {
  partnerName : string
}
@Injectable({
  providedIn: 'root'
})
export class OperationService {

  constructor(private  http: HttpClient) { }

  createOperation(operation: Operation)
  {
    return this.http.post('/api/operations', operation);
  }

  getCampaignOperations(campaignId : number) : Observable<OperationExtended[]>
  {
    return this.http.get('/api/campaigns/'+ campaignId + '/operations?order[operationDate]=desc').pipe(
        pluck("hydra:member"),
        map((data) => (data as Array<any>).map(
            (x) => (
                {
                  iri : x['@id'],
                  operationDate : x['operationDate'],
                  partnerName : x['partner']['name'],
                  sum : x['sum'],
                  comments : x['comments']
                } as OperationExtended)
        ))

    );
  }

  public deleteOperation(iri : string)
  {
      return this.http.delete(iri);
  }
}
