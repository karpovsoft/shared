import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Partner } from '../edit-partner/edit-partner.component'
import {Observable} from "rxjs";
import {pluck, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(private http: HttpClient) { }

  createParter(partner: Partner) : Observable<any>
  {
    return this.http.post("/api/partners", partner);
  }

  getTeamPartners(teamId: number) : Observable<Partner[]>
  {
    return this.http.get("/api/teams/"+ teamId + "/partners").pipe(
        pluck("hydra:member"),
        map((data) => (data as Array<any>).map(
            (x) => ({ iri : x['@id'], name : x['name']} as Partner)
        ))
    );
  }

}
