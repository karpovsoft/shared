import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SummaryService {

  constructor(private http : HttpClient) { }

  getSummary() : Observable<any> {
    return this.http.get("/api/summary");
  }

  getHistory() : Observable<any> {
    return this.http.get("/api/history")
  }
}
