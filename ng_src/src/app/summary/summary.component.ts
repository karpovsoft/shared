import { Component, OnInit } from '@angular/core';
import { SummaryService } from '../services/summary.service'
import {CurrentUserService, Settings} from "../services/currentuser.service";
import { MatDialog} from "@angular/material/dialog";
import {EditPartnerComponent} from "../edit-partner/edit-partner.component";
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from "@ngx-translate/core";
import {SettingsService} from "../services/settings.service";
import {EditOperationComponent} from "../edit-operation/edit-operation.component";
import {Observable, of} from "rxjs";

export class Summary {
  totalIncome : number = 0
  totalExpenses: number = 0
  startDate: string = ""
  totalPartners: number = 0
  status: string = ""
  partnerTotals: Array<any> = new Array<any>()
}

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  public summary : Summary = new Summary();
  public balance : Array<any> = Array<any>();

  constructor(
      private service: SummaryService,
      public currentUser: CurrentUserService,
      private dialog: MatDialog,
      public snackBar: MatSnackBar,
      private translate: TranslateService,
      private settings : SettingsService) { }

  public reloadOperations : number = Date.now();
  ngOnInit(): void {

    this.settings.getSettings().subscribe(
        resp => {
            this.currentUser.saveSettings(resp as Settings);
        },
        error => {
          this.snackBar.open(error.message);
        }
    );

    this.reloadSummary();
  }

  reloadSummary()
  {
    this.service.getSummary().subscribe(
        resp =>  {
          this.summary = resp;
          this.balance = this.summary.partnerTotals;
        },
        error => {
          this.snackBar.open(error.message);
        }
    )
    this.reloadOperations = Date.now(); // to fire ngChanges in child component
  }

  addPartner()
  {
    if(this.currentUser.hasRole("ROLE_ADMIN")) {
      let dialogRef = this.dialog.open(EditPartnerComponent);
      dialogRef.afterClosed().subscribe( result => {
        if(result == 'save') {
          this.reloadSummary();
          this.snackBar.open(this.translate.instant("form.saved"), "", {
            duration: 3000
          });
        }
      })
    }
  }

  addOperation()
  {
    if(this.currentUser.hasRole("ROLE_ADMIN")) {
      let dialogRef = this.dialog.open(EditOperationComponent);
      dialogRef.afterClosed().subscribe( result => {
        if(result == 'save') {
          this.reloadSummary();
          this.snackBar.open(this.translate.instant("form.saved"), "", {
            duration: 3000
          });
        }
      })
    }
  }

}
